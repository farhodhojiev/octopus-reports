﻿SET IDENTITY_INSERT [CollateralProducts] ON
INSERT INTO [CollateralProducts] ([id],[name],[desc],[deleted]) VALUES (1000,N'Gold (Тилло)',N'(GOLD)',0);
INSERT INTO [CollateralProducts] ([id],[name],[desc],[deleted]) VALUES (2000,N'Home contents (Амволи хона)',N'(HOMEASSETS)',0);
INSERT INTO [CollateralProducts] ([id],[name],[desc],[deleted]) VALUES (3000,N'Real estate (Амволи ғайриманқул)',N'(IMMOVABLES)',0);
INSERT INTO [CollateralProducts] ([id],[name],[desc],[deleted]) VALUES (4000,N'Goods in turnover (Молҳои дар гардиш қарордошта)',N'(TURNOVER)',0);
INSERT INTO [CollateralProducts] ([id],[name],[desc],[deleted]) VALUES (5000,N'Future harvest (Ҳосили оянда)',N'(FUTUREHARVEST)',0);
INSERT INTO [CollateralProducts] ([id],[name],[desc],[deleted]) VALUES (6000,N'Equipment (Таҷҳизот)',N'(EQUIPMENT)',0);
INSERT INTO [CollateralProducts] ([id],[name],[desc],[deleted]) VALUES (7000,N'Vehicles (Восити нақлиет)',N'(CAR)',0);
SET IDENTITY_INSERT [CollateralProducts] OFF