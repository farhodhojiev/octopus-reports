ALTER FUNCTION fnNumberToWordsEN(@NumberToWord as BIGINT)
    RETURNS NVARCHAR(1024)
AS
BEGIN 
      DECLARE @Below20 TABLE (ID int identity(0,1), Word varchar(32))
      DECLARE @Below100 TABLE (ID int identity(2,1), Word varchar(32))
      INSERT @Below20 (Word) VALUES
                        ( 'zero'), ('one'),( 'two' ), ( 'three'),
                        ( 'four' ), ( 'five' ), ( 'six' ), ( 'seven' ),
                        ( 'eight'), ( 'nine'), ( 'ten'), ( 'eleven' ),
                        ( 'twelve' ), ( 'thirteen' ), ( 'fourteen'),
                        ( 'fifteen' ), ('sixteen' ), ( 'seventeen'),
                        ('eighteen' ), ( 'nineteen' )
       INSERT @Below100 VALUES ('twenty'), ('thirty'),('forty'), ('fifty'),
                               ('sixty'), ('seventy'), ('eighty'), ('ninety')
DECLARE @English NVARCHAR(1024) =
(
  SELECT Case
    WHEN @NumberToWord = 0 THEN  'zero'
    WHEN @NumberToWord BETWEEN 1 AND 19
      THEN (SELECT Word FROM @Below20 WHERE ID=@NumberToWord)
   WHEN @NumberToWord BETWEEN 20 AND 99  
     THEN  (SELECT Word FROM @Below100 WHERE ID=@NumberToWord/10)+ '-' +
           dbo.fnNumberToWordsEN( @NumberToWord % 10)
   WHEN @NumberToWord BETWEEN 100 AND 999  
     THEN  (dbo.fnNumberToWordsEN( @NumberToWord / 100))+' hundred '+
         dbo.fnNumberToWordsEN( @NumberToWord % 100)
   WHEN @NumberToWord BETWEEN 1000 AND 999999  
     THEN  (dbo.fnNumberToWordsEN( @NumberToWord / 1000))+' thousand '+
         dbo.fnNumberToWordsEN( @NumberToWord % 1000) 
   WHEN @NumberToWord BETWEEN 1000000 AND 999999999  
     THEN  (dbo.fnNumberToWordsEN( @NumberToWord / 1000000))+' million '+
         dbo.fnNumberToWordsEN( @NumberToWord % 1000000)
        ELSE ' INVALID INPUT' END
)
SELECT @English = RTRIM(@English)
SELECT @English = RTRIM(LEFT(@English,len(@English)-1))
                 WHERE RIGHT(@English,1)='-'
                 
RETURN (@English)
END
GO