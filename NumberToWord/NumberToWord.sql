ALTER FUNCTION fnNumberToWords(@NumberToWordber AS DECIMAL(38, 4), @Language AS VARCHAR(4), @Currency AS NVARCHAR(10),
									@Usecents AS TINYINT, @CurrencyCents AS NVARCHAR(10))
    RETURNS NVARCHAR(1024)
AS
BEGIN
DECLARE @NumberToWordberWord AS NVARCHAR(150);
DECLARE @NumberToWordberCents AS INT
DECLARE @CentTJ AS NVARCHAR(5)

IF @Language='en'
	SET @NumberToWordberWord=dbo.fnNumberToWordsEN(@NumberToWordber)
ELSE IF @Language='ru'
	SET @NumberToWordberWord=dbo.fnNumberToWordsRU(@NumberToWordber, 1)
ELSE IF @Language='tj'
	SET @NumberToWordberWord=dbo.fnNumberToWordsTJ(@NumberToWordber)
ELSE SET @NumberToWordberWord=@Language+ ' not valid'


IF @Usecents=1
	BEGIN
	SET @NumberToWordberCents=100*(@NumberToWordber-ROUND(@NumberToWordber, 0, 1))
	SET @NumberToWordberWord=@NumberToWordberWord+' '+@Currency+' '+CONVERT(VARCHAR(3),@NumberToWordberCents)+' '+@CurrencyCents
	END
ELSE IF @Usecents=2
	BEGIN
	IF @Language='tj'
		BEGIN
		SET @centTJ=RIGHT(RTRIM(@NumberToWordberWord),2) -- определяем окончания 2-х последных символов
		
		IF (@centTJ='ду' OR @centTJ='се' OR @centTJ='си') AND (@NumberToWordber-ROUND(@NumberToWordber, 0, 1))>0
			SET @NumberToWordberWord=RTRIM(@NumberToWordberWord)+'ву'
		ELSE IF (@NumberToWordber-ROUND(@NumberToWordber, 0, 1))>0
			SET @NumberToWordberWord=RTRIM(@NumberToWordberWord)+'у'
		
		SET @NumberToWordberWord=@NumberToWordberWord+' '+dbo.fnNumberToWordsTJ_drob(@NumberToWordber)+' '+@Currency
		END
	ELSE IF @Language='ru' SET @NumberToWordberWord=@NumberToWordberWord+' '+dbo.fnNumberToWordsRU_drob(@NumberToWordber)+' '+@Currency
	ELSE IF @Language='en' SET @NumberToWordberWord=@NumberToWordberWord+' '+dbo.fnNumberToWordsEN_drob(@NumberToWordber)+' '+@Currency
	END
ELSE SET @NumberToWordberWord=@NumberToWordberWord+' '+@Currency

SET @NumberToWordberWord=RTRIM(@NumberToWordberWord)
SET @NumberToWordberWord=UPPER(LEFT(@NumberToWordberWord, 1))+RIGHT(@NumberToWordberWord, LEN(@NumberToWordberWord)-1)

RETURN (@NumberToWordberWord)
END
GO