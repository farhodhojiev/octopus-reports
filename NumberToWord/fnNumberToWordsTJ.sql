ALTER FUNCTION fnNumberToWordsTJ(@NumberToWord as BIGINT)
    RETURNS NVARCHAR(1024)
AS
BEGIN 
      DECLARE @Below20 TABLE (ID int identity(0,1), Word Nvarchar(32))
      DECLARE @Below100 TABLE (ID int identity(2,1), Word Nvarchar(32))
      INSERT @Below20 (Word) VALUES
                        ( N'сифр' ), ( N'як' ), ( N'ду' ), ( N'се'),
                        ( N'чор' ), ( N'панҷ' ), ( N'шаш' ), ( N'ҳафт' ),
                        ( N'ҳашт' ), ( N'нӯҳ' ), ( N'даҳ' ), ( N'ёздаҳ' ),
                        ( N'дувоздаҳ' ), ( N'сенздаҳ' ), ( N'чордаҳ'),
                        ( N'понздаҳ' ), ( N'шонздаҳ' ), ( N'ҳабдаҳ' ),
                        ( N'ҳаждаҳ' ), ( N'нуздаҳ' )
       INSERT @Below100 VALUES ( N'бист' ), ( N'си' ), ( N'чил' ), ( N'панҷоҳ' ),
                               ( N'шаст' ), ( N'ҳафтод' ), ( N'ҳаштод' ), ( N'навад' )
DECLARE @Tajik NVARCHAR(1024) =
(
  SELECT Case
    WHEN @NumberToWord = 0 THEN  N'сифр'
    WHEN @NumberToWord BETWEEN 1 AND 19
      THEN (SELECT Word FROM @Below20 WHERE ID=@NumberToWord)
      
   WHEN (@NumberToWord % 10)=0 AND @NumberToWord<99
     THEN  (SELECT Word FROM @Below100 WHERE ID=@NumberToWord/10)
   
   WHEN @NumberToWord BETWEEN 31 AND 39  
     THEN  (SELECT Word FROM @Below100 WHERE ID=@NumberToWord/10)+ N'ву ' +
           dbo.fnNumberToWordsTJ( @NumberToWord % 10)
                   
   WHEN @NumberToWord BETWEEN 20 AND 99  
     THEN  (SELECT Word FROM @Below100 WHERE ID=@NumberToWord/10)+ N'у ' +
           dbo.fnNumberToWordsTJ( @NumberToWord % 10)
           
   WHEN (@NumberToWord % 100)=0 AND @NumberToWord<999
     THEN  (dbo.fnNumberToWordsTJ( @NumberToWord / 100))+ N'сад '
         
   WHEN @NumberToWord BETWEEN 100 AND 999  
     THEN  (dbo.fnNumberToWordsTJ( @NumberToWord / 100))+ N'саду '+
         dbo.fnNumberToWordsTJ( @NumberToWord % 100)
         
   WHEN (@NumberToWord % 1000)=0 AND @NumberToWord<999999
     THEN  (dbo.fnNumberToWordsTJ( @NumberToWord / 1000))+ N' ҳазор'
     
   WHEN @NumberToWord BETWEEN 1000 AND 999999  
     THEN  (dbo.fnNumberToWordsTJ( @NumberToWord / 1000))+ N' ҳазору '+
         dbo.fnNumberToWordsTJ( @NumberToWord % 1000) 
         
   WHEN (@NumberToWord % 1000000)=0 AND @NumberToWord<999999999
     THEN  (dbo.fnNumberToWordsTJ( @NumberToWord / 1000000))+ N' миллион'
         
   WHEN @NumberToWord BETWEEN 1000000 AND 999999999  
     THEN  (dbo.fnNumberToWordsTJ( @NumberToWord / 1000000))+ N' миллиону '+
         dbo.fnNumberToWordsTJ( @NumberToWord % 1000000)
        ELSE ' INVALID INPUT' END
)
                 
SET @Tajik=REPLACE(@Tajik, N'яксад ', N'сад ')
RETURN (@Tajik)
END
GO