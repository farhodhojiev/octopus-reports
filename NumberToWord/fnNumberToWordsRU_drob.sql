CREATE FUNCTION fnNumberToWordsRU_drob(@NumberToWord as DECIMAL(10, 4))
    RETURNS NVARCHAR(1024)
AS
BEGIN 
DECLARE @drob NVARCHAR(1024)
DECLARE @drob_nol VARCHAR(1)
DECLARE @dahi INT
--DECLARE @drob_butun AS DECIMAL(38, 4)
               
SET @drob=CAST((@NumberToWord-ROUND(@NumberToWord, 0, 1)) AS NVARCHAR(8))
SET @drob=RIGHT(@drob, LEN(@drob)-2)
SET @drob_nol=RIGHT(@drob, 1)
SET @dahi=1

WHILE @drob_nol='0'
	BEGIN
	SET @drob=LEFT(@drob, LEN(@drob)-1)
	SET @drob_nol=RIGHT(@drob, 1)
	SET @dahi=@dahi+1
	END

IF @dahi=1 SET @drob='целых и '+dbo.fnNumberToWordsRU(CAST(@drob AS INT), 1)+' десятитысячных'
ELSE IF @dahi=2 SET @drob='целых и '+dbo.fnNumberToWordsRU(CAST(@drob AS INT), 1)+' тысячных'
ELSE IF @dahi=3 SET @drob='целых и '+dbo.fnNumberToWordsRU(CAST(@drob AS INT), 1)+' сотых'
ELSE IF @dahi=4 SET @drob='целых и '+dbo.fnNumberToWordsRU(CAST(@drob AS INT), 1)+' десятых'

RETURN (@drob)
END
GO