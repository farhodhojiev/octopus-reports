ALTER FUNCTION fnNumberToWordsTJ_drob(@NumberToWord as DECIMAL(10, 4))
    RETURNS NVARCHAR(1024)
AS
BEGIN 
DECLARE @drob NVARCHAR(1024)
DECLARE @drob_nol NVARCHAR(1)
DECLARE @dahi INT
--DECLARE @drob_butun AS DECIMAL(38, 4)
               
SET @drob=CAST((@NumberToWord-ROUND(@NumberToWord, 0, 1)) AS NVARCHAR(8))
SET @drob=RIGHT(@drob, LEN(@drob)-2)
SET @drob_nol=RIGHT(@drob, 1)
SET @dahi=1

WHILE @drob_nol='0'
	BEGIN
	SET @drob=LEFT(@drob, LEN(@drob)-1)
	SET @drob_nol=RIGHT(@drob, 1)
	SET @dahi=@dahi+1
	END

IF @dahi=1 SET @drob=N'аз даҳ ҳазор '+dbo.fnNumberToWordsTJ(CAST(@drob AS INT))
ELSE IF @dahi=2 SET @drob=N'аз ҳазор '+dbo.fnNumberToWordsTJ(CAST(@drob AS INT))
ELSE IF @dahi=3 SET @drob=N'аз сад '+dbo.fnNumberToWordsTJ(CAST(@drob AS INT))
ELSE IF @dahi=4 SET @drob=N'аз даҳ '+dbo.fnNumberToWordsTJ(CAST(@drob AS INT))

RETURN (@drob)
END
GO