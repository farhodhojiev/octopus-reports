USE [vcvc]
GO

/****** Object:  Table [dbo].[InfoOTJ]    Script Date: 01/09/2012 11:17:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[InfoOTJ](
	[id] [int] NULL,
	[branch_code] [nchar](5) NULL,
	[name_of_branch] [nchar](50) NULL,
	[branch_manager] [nchar](50) NULL,
	[branch_address] [nchar](100) NULL,
	[branch_city] [nchar](50) NULL,
	[branch_i_caption] [nchar](500) NULL,
	[branch_g_caption] [nchar](500) NULL,
	[doljnost_direktora] [nchar](50) NULL,
	[doljnost_gde] [nchar](50) NULL,
	[doljnost_gde_address] [nchar](50) NULL,
	[deleted] [bit] NULL
) ON [PRIMARY]

GO


