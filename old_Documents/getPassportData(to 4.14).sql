ALTER FUNCTION [dbo].[getPassportData]
    (
      @PersonId INT
    )
RETURNS NVARCHAR(100)
AS BEGIN
   RETURN  (
	SELECT per.identification_data+' '+
	ISNULL((SELECT AfV.value FROM AdvancedFieldsLinkEntities AfL
	LEFT JOIN AdvancedFieldsValues AfV ON AfV.entity_field_id=AfL.id
	WHERE Afl.link_id=per.id AND AfL.link_type='C' AND AfV.field_id=800), ' ')+' аз '+
	ISNULL((SELECT CONVERT(NVARCHAR(10), CAST(AfV.value AS DATE), 104) FROM AdvancedFieldsLinkEntities AfL
	LEFT JOIN AdvancedFieldsValues AfV ON AfV.entity_field_id=AfL.id
	WHERE Afl.link_id=per.id AND AfL.link_type='C' AND AfV.field_id=801), ' ')+' сол'
	FROM Persons per WHERE per.id=@PersonId
    )
END
